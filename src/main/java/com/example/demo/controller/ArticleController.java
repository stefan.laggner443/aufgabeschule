package com.example.demo.controller;

import com.example.demo.model.Article;
import com.example.demo.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @GetMapping("/articles")
    public List<Article> getAllArticles() {
        return articleService.listAll();
    }

    @RequestMapping("/articles/{id}")
    public Article getArticle(@PathVariable(name = "id") Long id) {
        return articleService.get(id);
    }

    @RequestMapping(value= "/articles/save", method = RequestMethod.POST)
    public String saveArticle(@Valid @ModelAttribute("article") Article article) {
        articleService.save(article);
        return null;
    }

    @RequestMapping("/articles/delete/{id}")
    public String deleteArticle(@PathVariable(name = "id") Long id) {
        articleService.delete(id);
        return null;
    }
}
