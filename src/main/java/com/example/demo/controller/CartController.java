package com.example.demo.controller;

import com.example.demo.model.Cart;
import com.example.demo.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@RestController
public class CartController {
    @Autowired
    private CartService cartService;

    @GetMapping("/carts")
    public List<Cart> getAllCarts() {
        return cartService.listAll();
    }

    @RequestMapping("/carts/{id}")
    public Cart getCart(@PathVariable(name = "id") Long id) {
        return cartService.get(id);
    }

    @RequestMapping(value= "/carts/save", method = RequestMethod.POST)
    public String saveCart(@Valid @ModelAttribute("cart") Cart article) {
        cartService.save(article);
        return null;
    }

    @RequestMapping("/carts/delete/{id}")
    public String deleteCart(@PathVariable(name = "id") Long id) {
        cartService.delete(id);
        return null;
    }
}
