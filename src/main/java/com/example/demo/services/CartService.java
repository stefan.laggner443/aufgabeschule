package com.example.demo.services;

import com.example.demo.model.Cart;
import com.example.demo.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartService {
    @Autowired
    private CartRepository repo;

    public List<Cart> listAll() {
        return repo.findAll();
    }

    public Cart save(Cart article) {
        repo.save(article);
        return article;
    }

    public Cart get(Long id) {
        return repo.findById(id).get();
    }

    public void delete(Long id) {
        repo.deleteById(id);
    }
}
