import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import Select from 'react-select'

class App extends Component {
    state = {
        loading: true,
        articles: [],
        crud: "view",
        editId: false,
        articleName: "",
        articleDescription: "",
        articlePrice: "",
        articleSection: "",
        carts: []
    };

    async componentDidMount() {
        var formData = new FormData();
        formData.append('name', "test");
        formData.append('description', "test");
        formData.append('price', 123);
        formData.append('cart', 1);
        axios.post("/articles/save", formData).then((result) => {
        console.log(result)
        })



        axios.get("/articles").then((result) => {
            this.setState({articles: result.data})
        })

        axios.get("/carts").then((result) => {
            let result1 = result.data.map((e) => {
                return {label: e.section, value: e.id}
            })
            this.setState({carts: result1, loading: false})
        })

    }

    update(update = false) {
        var formData = new FormData();
        update ? formData.append('id', this.state.editId) : <div></div>;
        formData.append('name', this.state.articleName);
        formData.append('description', this.state.articleDescription);
        formData.append('price', this.state.articlePrice);
        formData.append('cart', this.state.articleSection.id);
        axios.post("/articles/save", formData).then((result) => {
            console.log(result)
            this.setState(previousState => ({
                crud: "view",
                articles: update ? this.state.articles.map(el => (el.id === this.state.editId ? {
                    ...el,
                    name: this.state.articleName,
                    description: this.state.articleDescription,
                    price: this.state.articlePrice,
                    cart: this.state.articleSection
                } : el)) : [...previousState.articles, {
                    name: this.state.articleName,
                    description: this.state.articleDescription,
                    price: this.state.articlePrice,
                    cart: this.state.articleSection
                }]
            }))
        })
    }

    delete(id) {
        axios.delete("/articles/delete/" + id).then(() => {
            this.setState({
                articles: this.state.articles.filter((e) => {
                    return e.id !== id
                })
            })
        })
    }

    render() {
        if (!this.state.loading) {
            let article = this.state.articles.find((e) => {
                return e.id === this.state.editId
            })

            let defaultSelect = article ? article.cart ? {
                label: article.cart.section,
                value: article.cart.id
            } : {label: "Bitte wählen", value: 0} : {label: "Bitte wählen", value: 0}

            switch (this.state.crud) {
                case "view":
                    return (
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kategorie</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.articles.map((e) => {
                                return <tr key={e.id}>
                                    <td>{e.id}</td>
                                    <td>{e.cart ? e.cart.section : "Nicht zugeteilt"}</td>
                                    <td>{e.name}</td>
                                    <td>{e.description}</td>
                                    <td>{e.price}</td>
                                    <td onClick={() => this.delete(e.id)}>Delete Me</td>
                                    <td onClick={() => this.setState({
                                        crud: "edit",
                                        editId: e.id,
                                        articleName: e.name,
                                        articleDescription: e.description,
                                        articlePrice: e.price,
                                        articleSection: e.cart ? e.cart : null
                                    })}>Edit Me
                                    </td>
                                </tr>
                            })}
                            </tbody>
                        </table>
                    );
                case "edit":
                    return <div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Kategorie</label>
                            <Select options={this.state.carts} defaultValue={defaultSelect}
                                    onChange={(e) => this.setState({articleSection: {id: e.value, section: e.label}})}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Name</label>
                            <input type="text" className="form-control"
                                   onChange={(e) => this.setState({articleName: e.target.value})}
                                   defaultValue={article ? article.name : ""}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Beschreibung</label>
                            <input type="text" className="form-control"
                                   onChange={(e) => this.setState({articleDescription: e.target.value})}
                                   defaultValue={article ? article.description : ""}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Preis</label>
                            <input type="text" className="form-control"
                                   onChange={(e) => this.setState({articlePrice: e.target.value})}
                                   defaultValue={article ? article.price : ""}/>
                        </div>
                        <button type="submit" className="btn btn-primary" onClick={() => this.update(true)}>Speichern
                        </button>
                    </div>
                case "create":
                    return <div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Kategorie</label>
                            <Select options={this.state.carts} defaultValue={defaultSelect}
                                    onChange={(e) => this.setState({articleSection: {id: e.value, section: e.label}})}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Name</label>
                            <input type="text" className="form-control"
                                   onChange={(e) => this.setState({articleName: e.target.value})}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Beschreibung</label>
                            <input type="text" className="form-control"
                                   onChange={(e) => this.setState({articleDescription: e.target.value})}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Preis</label>
                            <input type="text" className="form-control"
                                   onChange={(e) => this.setState({articlePrice: e.target.value})}
                            />
                        </div>
                        <button type="submit" className="btn btn-primary" onClick={() => this.insert()}>Speichern
                        </button>
                    </div>
            }
        } else {
            return "Bitte warten..."
        }
    }
}

export default App;